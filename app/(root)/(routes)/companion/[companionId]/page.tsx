import prismadb from "@/lib/prismadb";
import { CompanionForm } from "./components/companion-form";
import { Companion } from "@prisma/client";
import { auth, redirectToSignIn } from "@clerk/nextjs";

interface CompanionIdPageProps {
  params: {
    companionId: string;
  };
}

const CompanionIdPage = async ({ params }: CompanionIdPageProps) => {
  const { userId } = auth();
  // TODO check subscription

  if (!userId) {
    return redirectToSignIn();
  }
  let companion: Companion | null = null;
  try {
    companion = await prismadb.companion.findUniqueOrThrow({
      where: {
        userId,
        id: params.companionId,
      },
    });
  } catch (e) {
    console.log(e);
  }

  const categories = await prismadb.category.findMany();

  return (
    <>
      <CompanionForm initialData={companion} categories={categories} />
    </>
  );
};

export default CompanionIdPage;
