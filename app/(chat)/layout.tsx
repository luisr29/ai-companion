const Chatlayout = ({ children }: { children: React.ReactNode }) => {
  return <div className="h-full w-full mx-auto max-w-4xl">{children}</div>;
};

export default Chatlayout;
